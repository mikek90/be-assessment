video_qualities = VideoQuality.create([{ name: 'SD'}, { name: 'HD'}])

movies = Movie.create([
  { name: 'Star Wars', plot: 'The galaxy is at a period of a civil war...' },
  { name: 'Lord of the Rings', plot: 'Thousands of years before the events of the novel, the Dark Lord Sauron had forged the One Ring to rule the other Rings of Power and corrupt those who wore them: three for Elves, seven for Dwarves, and nine for Men.' }
])

seasons = Season.create([
  { name: 'First season of Breaking Bad', plot: Faker::Lorem.paragraph(sentence_count: 3), number: 1 },
  { name: 'Second season of Breaking Bad', plot: Faker::Lorem.paragraph(sentence_count: 3), number: 2 },
  { name: 'Third season of Breaking Bad', plot: Faker::Lorem.paragraph(sentence_count: 3), number: 3 },
  { name: 'First season of Better Call Saul', plot: Faker::Lorem.paragraph(sentence_count: 3), number: 1 }
])

episodes = []

(1..7).to_a.shuffle.each do |episode_no|
  episodes << { name: "Breaking Bad S01E0#{episode_no}", plot: "Plot 1-#{episode_no}", number: episode_no, season: seasons.first }
end

(1..13).to_a.shuffle.each do |episode_no|
  episodes << { name: "Breaking Bad S02E0#{episode_no}", plot: "Plot 2-#{episode_no}", number: episode_no, season: seasons.second }
end

(1..13).to_a.shuffle.each do |episode_no|
  episodes << { name: "Breaking Bad S03E0#{episode_no}", plot: "Plot 3-#{episode_no}", number: episode_no, season: seasons.third }
end

(1..10).to_a.shuffle.each do |episode_no|
  episodes << { name: "Better Call Saul S01E0#{episode_no}", plot: "Plot 1-#{episode_no}", number: episode_no, season: seasons.fourth }
end

Episode.create(episodes)

User.create((1..10).map {{ email: Faker::Internet.unique.email }})

# more random stuff
random_movies = Movie.create((1..7).map {|i| { name: Faker::Movies::HarryPotter.unique.book, plot: Faker::Movies::HarryPotter.unique.quote }})
random_seasons = []

(1..10).each do |season_no|
  planet = Faker::Movies::StarWars.unique.planet
  season = Season.create({ name: "Wookie on #{planet}", plot: Faker::Lorem.paragraph(sentence_count: 3), number: season_no})
  random_seasons << season

  (1..Random.rand(7..15)).each do |episode_no|
    Episode.create({ name: Faker::Movies::StarWars.unique.wookiee_sentence, plot: Faker::Movies::StarWars.unique.quote, number: episode_no, season: season })
  end
end

movies.concat(seasons).concat(random_movies).concat(random_seasons).each_with_index do |content, index|
  PurchaseOption.create({content: content, video_quality: video_qualities.first, price: 2.99})
  PurchaseOption.create({content: content, video_quality: video_qualities.second, price: 2.99}) if index%2 == 1
end
