class CreatePurchaseOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_options do |t|
      t.references :video_quality, foreign_key: true, null: false
      t.decimal :price, precision: 5, scale: 2, null: false
      t.references :content, foreign_key: true, null: false

      t.timestamps
    end
  end
end
