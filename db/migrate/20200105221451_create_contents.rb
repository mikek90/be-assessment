class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :name, null: false
      t.string :plot, null: false
      t.bigint :parent_id
      t.integer :number
      t.string :type, null: false

      t.timestamps

      t.foreign_key :contents, column: :parent_id

      t.index :created_at
      t.index :type
      t.index :number
    end
  end
end
