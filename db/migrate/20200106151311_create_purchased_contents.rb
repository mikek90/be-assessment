class CreatePurchasedContents < ActiveRecord::Migration[5.2]
  def change
    create_table :purchased_contents do |t|
      t.references :user, foreign_key: true, null: false
      t.references :purchase_option, foreign_key: true, null: false
      t.decimal :paid, precision: 5, scale: 2, null: false

      t.timestamps
    end
  end
end
