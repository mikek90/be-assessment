# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_06_151311) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contents", force: :cascade do |t|
    t.string "name", null: false
    t.string "plot", null: false
    t.bigint "parent_id"
    t.integer "number"
    t.string "type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at"], name: "index_contents_on_created_at"
    t.index ["number"], name: "index_contents_on_number"
    t.index ["type"], name: "index_contents_on_type"
  end

  create_table "purchase_options", force: :cascade do |t|
    t.bigint "video_quality_id", null: false
    t.decimal "price", precision: 5, scale: 2, null: false
    t.bigint "content_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["content_id"], name: "index_purchase_options_on_content_id"
    t.index ["video_quality_id"], name: "index_purchase_options_on_video_quality_id"
  end

  create_table "purchased_contents", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "purchase_option_id", null: false
    t.decimal "paid", precision: 5, scale: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["purchase_option_id"], name: "index_purchased_contents_on_purchase_option_id"
    t.index ["user_id"], name: "index_purchased_contents_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "video_qualities", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_video_qualities_on_name", unique: true
  end

  add_foreign_key "contents", "contents", column: "parent_id"
  add_foreign_key "purchase_options", "contents"
  add_foreign_key "purchase_options", "video_qualities"
  add_foreign_key "purchased_contents", "purchase_options"
  add_foreign_key "purchased_contents", "users"
end
