class PurchaseOptionValidator < ActiveModel::Validator
  def validate(record)
    unless Content::PURCHASABLE_TYPES.include? record.content.type
      record.errors[:content_id] << 'Content is not purchasable type!'
    end
  end
end

class PurchaseOption < ApplicationRecord
  scope :with_video_quality, -> { includes(:video_quality) }

  belongs_to :video_quality
  belongs_to :content

  validates_with PurchaseOptionValidator
end
