class Episode < Content
  belongs_to :season, class_name: 'Season', foreign_key: 'parent_id'

  validates :parent_id, :number, presence: true
  validates :number, uniqueness: { scope: [:parent_id] }
end
