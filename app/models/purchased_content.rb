class PurchasedContentValidator < ActiveModel::Validator
  def validate(record)
    existing = PurchasedContent.active.find_by({user_id: record.user_id, purchase_option_id: record.purchase_option_id})
    record.errors[:purchase_option_id] << "Current purchase exists in database." unless existing.blank?
  end
end

class PurchasedContent < ApplicationRecord
  belongs_to :user
  belongs_to :purchase_option

  scope :active, -> { order(:created_at).where("created_at >= ?", 2.days.ago).includes(purchase_option: [:video_quality, content: [:episodes]]) }

  validates_with PurchasedContentValidator
end
