class User < ApplicationRecord
  has_many :purchased_contents, -> { active }
  
  validates :email, uniqueness: true, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }
end
