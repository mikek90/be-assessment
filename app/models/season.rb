class Season < Content
  has_many :purchase_options, -> { with_video_quality }, foreign_key: 'content_id'

  validates :parent_id, absence: true
  validates :number, presence: true
  validates :number, uniqueness: { scope: [:name] }
end
