class Content < ApplicationRecord
  MOVIE_TYPE = "Movie".freeze
  SEASON_TYPE = "Season".freeze
  EPISODE_TYPE = "Episode".freeze

  PURCHASABLE_TYPES = [MOVIE_TYPE, SEASON_TYPE].freeze

  has_many :episodes, -> { order(:number) }, class_name: 'Episode', foreign_key: 'parent_id'

  scope :by_created_at, -> { order(:created_at) }
  scope :purchasable, -> { where(type: PURCHASABLE_TYPES) }
end
