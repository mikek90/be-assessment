class PurchasedContentsController < ApplicationController
  before_action :dummy_authorize

  # GET /purchased_contents
  # GET /purchased_contents.json
  def index
    @purchased_contents = PurchasedContent.active.where(user_id: @user.id)
    @purchased_contents = @purchased_contents.page(@page).per(@page_size) unless @page.blank?
  end

  # POST /purchased_contents
  # POST /purchased_contents.json
  def create
    purchase_option = PurchaseOption.find(params[:purchase_option_id])
    @purchased_content = PurchasedContent.new(purchased_content_params)
    @purchased_content.paid = purchase_option.price

    if @purchased_content.save
      render :show, status: :created, location: @purchased_content
    else
      render json: @purchased_content.errors, status: :unprocessable_entity
    end
  end

  # GET /purchased_contents/1
  # GET /purchased_contents/1.json
  def show
    @purchased_content = PurchasedContent.find(params[:id])
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def purchased_content_params
      params.require(:purchased_content).permit(:user_id, :purchase_option_id)
    end

    def dummy_authorize
      begin
        @user = User.find(params[:user_id])
      rescue ActiveRecord::RecordNotFound
        render json: { error: "Unauthorized." }, status: :unauthorized
      end
    end
end
