class ApplicationController < ActionController::API
  before_action :set_page_info, only: [:index]

  private

  def set_page_info
    @page = params[:page].presence
    @page_size = params[:pagesize].presence || 5 # CONSIDER moving to env variables
  end
end
