class WrongTypeException < StandardError; end

class ContentsController < ApplicationController
  before_action :set_type

  CACHE_EXPIRATION = 5.minutes.freeze
  ALL_REQUEST_TYPE = "All".freeze

  # GET /contents
  # GET /contents.json
  def index
    @contents = cached_contents
  rescue WrongTypeException
    render json: { error: "Wrong type", status: 400 }, status: 400 and return
  end

  private

  def cache_key
    "#{self.controller_name}_#{self.action_name}?#{@page}:#{@page_size}:#{@type}"
  end

  def permitted_params
    params.permit(:type)
  end

  def set_type
    @type = params[:type].capitalize
  end

  # CONSIDER moving it to service
  # caching make sense here only when we assume that contents database state is not changing in 5 minutes
  def cached_contents
    Rails.cache.fetch(cache_key, expires_in: CACHE_EXPIRATION) do
      case @type
      when Content::MOVIE_TYPE
        contents = Movie.includes(:purchase_options).by_created_at
      when Content::SEASON_TYPE
        contents = Season.includes(:episodes, :purchase_options).by_created_at
      when ALL_REQUEST_TYPE
        contents = Content.purchasable.includes(:episodes, :purchase_options).by_created_at
      else
        raise WrongTypeException
      end

      contents = contents.page(@page).per(@page_size) unless @page.blank?
      contents.to_a
    end
  end
end
