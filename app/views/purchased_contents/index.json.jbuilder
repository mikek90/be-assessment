json.items @purchased_contents do |purchased_content|
  purchase_option = purchased_content.purchase_option

  json.extract! purchased_content, :id

  json.paid                 purchased_content.paid.to_f
  json.createdAt            purchased_content.created_at

  json.purchaseOption do
    json.extract! purchase_option, :id

    json.videoQuality       purchase_option.video_quality.name
    json.price              purchase_option.price.to_f
    json.createdAt          purchase_option.created_at

    json.content do
      content = purchase_option.content

      json.extract! content, :id, :name, :plot, :type

      json.createdAt        content.created_at

      if (content.type == Content::SEASON_TYPE)
        json.extract! content, :number

        json.episodes content.episodes do |episode|
          json.extract! episode, :id, :name, :plot, :type, :number

          json.createdAt    episode.created_at
        end
      end
    end
  end
end

json.itemsCount            @purchased_contents.count

if @page.present?
  json.page                @page.to_i
  json.pageSize            @page_size.to_i
end
