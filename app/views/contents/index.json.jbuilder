json.items @contents do |content|
  json.extract! content, :id, :name, :plot, :type

  json.createdAt          content.created_at

  json.purchaseOptions content.purchase_options do |option|
    json.extract! option, :id

    json.price            option.price.to_f
    json.videoQuality     option.video_quality.name
  end

  if (content.type == Content::SEASON_TYPE)
    json.extract! content, :number

    json.episodes content.episodes do |episode|
      json.extract! episode, :id, :name, :plot, :type, :number

      json.createdAt      episode.created_at
    end
  end
end

json.itemsCount            @contents.count

if @page.present?
  json.page                @page.to_i
  json.pageSize            @page_size.to_i
end
