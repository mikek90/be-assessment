require 'rails_helper'

RSpec.describe "Contents", type: :request do
  describe "GET /contents" do
    it "works! (now write some real specs)" do
      ["movie", "season", "all"].each do |type|
        get "/contents/#{type}", as: :json
        expect(response).to have_http_status(200)
      end
    end
  end
end
