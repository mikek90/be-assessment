require 'rails_helper'

RSpec.describe "PurchasedContents", type: :request do
  describe "GET /purchased_contents" do
    before(:each) do
    end

    it "works! (now write some real specs)" do
      user = User.create({ email: "test@example.com" })
      get purchased_contents_path, params: { user_id: user.id }
      expect(response).to have_http_status(200)
    end

    it "returns unauthorized when user_id is not set" do
      get purchased_contents_path, as: :json
      expect(response).to have_http_status(401)
    end

    it "returns unauthorized when user_id is wrong" do
      get purchased_contents_path, params: { user_id: 55 }, as: :json
      get purchased_contents_path, as: :json
      expect(response).to have_http_status(401)
    end
  end
end
