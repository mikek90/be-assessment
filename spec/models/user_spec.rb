require "rails_helper"

RSpec.describe User, type: :model do
  describe "entity save" do
    it "should save new entity when email not exists in database and is valid" do
      email = "test@example.com"
      user = User.new({ email: email }).save

      expect(user).to eq true
    end

    it "should not save new entity when email exists in database" do
      email = "test@example.com"
      User.create({ email: email })

      duplicated_user = User.new({ email: email }).save
      expect(duplicated_user).to eq false
    end

    it "should not save new entity when email format is wrong" do
      duplicated_user = User.new({ email: "wrong mail" }).save
      expect(duplicated_user).to eq false
    end
  end
end
