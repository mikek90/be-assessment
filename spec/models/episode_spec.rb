require "rails_helper"

RSpec.describe Episode, type: :model do
  let(:movie) { Movie.create({ name: "m1", plot: "p-m1" }) }
  let(:season) { Season.create({ name: "s1", plot: "p-s1", number: 1 }) }
  let(:episode) { Episode.create({ name: "s1e1", plot: "p-s1e1", number: 1, season: season }) }

  describe "entity save" do
    it "should not save episode without parent_id and number set" do
      episode = Episode.new({ name: "m1", plot: "p-m1" })
      expect(episode.save).to eq false
    end

    it "should not save episode if content type is Movie" do
      expect {
        Episode.create({ name: "m1", plot: "p-m1", number: 1, season: movie })
      }.to raise_error(ActiveRecord::AssociationTypeMismatch)
    end

    it "should not save episode if content type is Episode" do
      expect {
        Episode.create({ name: "m1", plot: "p-m1", number: 1, season: episode })
      }.to raise_error(ActiveRecord::AssociationTypeMismatch)
    end

    it "should not save episode for the same season with existing number" do
      Episode.create({ name: "m1", plot: "p-m1", number: 1, season: season })
      episode = Episode.new({ name: "m1", plot: "p-m1", number: 1, season: season })
      expect(episode.save).to eq false
    end
  end
end
