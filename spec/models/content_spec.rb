require "rails_helper"

RSpec.describe Content, type: :model do
  it "should not save content without name and plot" do
    expect {
      Content.create({ number: 1 })
    }.to raise_error(ActiveRecord::NotNullViolation)
  end
end
