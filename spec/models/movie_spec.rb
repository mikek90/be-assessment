require "rails_helper"

RSpec.describe Movie, type: :model do
  let(:season) { Season.create({ name: "s1", plot: "p-s1", number: 1 }) }
  let(:episode) { Episode.create({ name: "s1e1", plot: "p-s1e1", number: 1, season: season }) }

  describe "entity save" do
    it "should not save movie with parent_id and number set" do
      movie = Movie.new({ name: "m1", plot: "p-m1", parent_id: season.id, number: 1 })
      expect(movie.save).to eq false
    end

    it "should not save movie with number set" do
      movie = Movie.new({ name: "m1", plot: "p-m1", number: 1 })
      expect(movie.save).to eq false
    end

    it "should not save movie with parent_id set" do
      movie = Movie.new({ name: "m1", plot: "p-m1", parent_id: season.id })
      expect(movie.save).to eq false
    end

    it "should save movie without parent_id and number set" do
      movie = Movie.new({ name: "m1", plot: "p-m1" })
      expect(movie.save).to eq true
    end
  end
end
