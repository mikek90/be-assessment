require "rails_helper"

RSpec.describe PurchaseOption, type: :model do
  let(:video_qualities) { VideoQuality.create([{ name: "SD"}, { name: "HD"}]) }
  let(:movie) { Movie.create({ name: "m1", plot: "p-m1" }) }
  let(:season) { Season.create({ name: "s1", plot: "p-s1", number: 1 }) }
  let(:episode) { Episode.create({ name: "s1e1", plot: "p-s1e1", number: 1, season: season }) }

  describe "entity save" do
    it "should save purchase option with movie content" do
      purchase_option = PurchaseOption.new({ video_quality: video_qualities.first, content: movie, price: 1.23 })
      expect(purchase_option.save).to eq true
    end

    it "should save purchase option with season content" do
      purchase_option = PurchaseOption.new({ video_quality: video_qualities.first, content: season, price: 1.23 })
      expect(purchase_option.save).to eq true
    end

    it "should not save purchase option with episode content" do
      purchase_option = PurchaseOption.new({ video_quality: video_qualities.first, content: episode, price: 1.23 })
      expect(purchase_option.save).to eq false
      expect(purchase_option.errors.messages[:content_id]).to eq(["Content is not purchasable type!"])
    end
  end
end
