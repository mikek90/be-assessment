require "rails_helper"

RSpec.describe Season, type: :model do
  describe "entity save" do
    let(:movie) { Movie.create({ name: "m1", plot: "p-m1" }) }

    it "should save season with parent_id set and without number set" do
      season = Season.new({ name: "s1", plot: "p-s1", number: 1 })
      expect(season.save).to eq true
    end

    it "should not save season with parent_id set" do
      season = Season.new({ name: "s1", plot: "p-s1", parent_id: movie.id })
      expect(season.save).to eq false
    end

    it "should not save season without number set" do
      season = Season.new({ name: "s1", plot: "p-s1" })
      expect(season.save).to eq false
    end

    it "should not save season with two the same names and two the same numbers" do
      Season.create({ name: "s1", plot: "p-s1", number: 1 })
      season = Season.new({ name: "s1", plot: "p-s1", number: 1 })
      expect(season.save).to eq false
    end
  end
end
