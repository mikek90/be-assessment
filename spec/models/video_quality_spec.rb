require "rails_helper"

RSpec.describe VideoQuality, type: :model do
  describe "entity save" do
    it "should save new entity when video quality name does not exist in database" do
      VideoQuality.create({ name: "test1" })

      duplicated_video_quality = VideoQuality.new({ name: "test2" }).save
      expect(duplicated_video_quality).to eq true
    end

    it "should not save new entity when video quality name exists in database" do
      name = "TEST"
      VideoQuality.create({ name: name })

      duplicated_video_quality = VideoQuality.new({ name: name }).save
      expect(duplicated_video_quality).to eq false
    end
  end
end
