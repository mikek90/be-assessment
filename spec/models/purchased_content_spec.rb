require "rails_helper"

RSpec.describe PurchasedContent, type: :model do
  let(:video_qualities) { VideoQuality.create([{ name: "SD"}, { name: "HD"}]) }
  let(:movies) {
    Movie.create([
      { name: "m1", plot: "p-m1" },
      { name: "m2", plot: "p-m2" }
    ])
  }
  let(:purchase_options) {[
    PurchaseOption.create({content: movies.first, video_quality: video_qualities.first, price: 2.99}),
    PurchaseOption.create({content: movies.second, video_quality: video_qualities.second, price: 2.99})
  ]}
  let(:user) { User.create({ email: "test@example.com" }) }

  it "should be able to purchase content" do
    purchased_content = PurchasedContent.new({user: user, purchase_option: purchase_options.first, paid: 2.99})
    expect(purchased_content.save).to eq true
  end

  it "should be able to purchase two different content options" do
    first_purchased_content = PurchasedContent.new({user: user, purchase_option: purchase_options.first, paid: 2.99})
    expect(first_purchased_content.save).to eq true

    second_purchased_content = PurchasedContent.new({user: user, purchase_option: purchase_options.second, paid: 2.99})
    expect(second_purchased_content.save).to eq true
  end

  it "should not be able to purchase two the same content options before two days" do
    first_purchased_content = PurchasedContent.new({user: user, purchase_option: purchase_options.first, paid: 2.99})
    expect(first_purchased_content.save).to eq true

    second_purchased_content = PurchasedContent.new({user: user, purchase_option: purchase_options.first, paid: 2.99})
    expect(second_purchased_content.save).to eq false

    expect(second_purchased_content.errors.messages[:purchase_option_id]).to eq(["Current purchase exists in database."])
  end

  it "should be able to purchase two the same content options after two days" do
    first_purchased_content = PurchasedContent.new({created_at: 3.days.before, user: user, purchase_option: purchase_options.first, paid: 2.99})
    expect(first_purchased_content.save).to eq true

    second_purchased_content = PurchasedContent.new({user: user, purchase_option: purchase_options.first, paid: 2.99})
    expect(second_purchased_content.save).to eq true
  end
end
