require 'rails_helper'

RSpec.describe PurchasedContentsController, type: :controller do

  let(:user) { User.create({ email: "test@example.com" }) }
  let(:video_quality) { VideoQuality.create({ name: 'HD'}) }
  let(:movie) { Movie.create({ name: 'Star Wars', plot: 'p sw' }) }
  let(:purchase_option) { PurchaseOption.create({content: movie, video_quality: video_quality, price: 2.99}) }

  describe "GET #index" do
    it "returns a success response" do
      get :index, params: {user_id: user.id}, as: :json
      expect(response).to be_successful
    end

    describe "pagination" do
      before(:each) do
        (1..7).each do |i|
          m = Movie.create({ name: "m#{i}", plot: "p m#{i}" })
          p = PurchaseOption.create({content: m, video_quality: video_quality, price: 2.99})
          PurchasedContent.create({ user_id: user.id, purchase_option_id: p.id, paid: p.price })
        end
      end

      it "returns all items if page parameter is not passed" do
        get :index, params: {user_id: user.id}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body['items'].count).to eq 7
        expect(response_body['items'].count).to eq response_body['itemsCount']
      end

      it "returns 5 items for first page if pagesize param is not set" do
        get :index, params: {user_id: user.id, page: 1}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body["items"].count).to eq 5
      end

      it "returns pagesize items for first page" do
        get :index, params: {user_id: user.id, page: 1, pagesize: 3}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body["items"].count).to eq 3
      end

      it "returns last page" do
        get :index, params: {user_id: user.id, page: 3, pagesize: 3}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body["items"].count).to eq 1
      end
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      purchased_content = PurchasedContent.create({ user_id: user.id, purchase_option_id: purchase_option.id, paid: purchase_option.price })
      get :show, params: {id: purchased_content.to_param, user_id: user.id}, as: :json
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    describe "purchase content option" do
      let(:request_params) {
        {user_id: user.id, purchase_option_id: purchase_option.id}
      }

      it "should purchase when user hadn't bought this content option before" do
        expect {
          post :create, params: request_params, as: :json
        }.to change(PurchasedContent, :count).by(1)
      end

      it "should not purchase when user bought this content earlier than two days before" do
        PurchasedContent.create({ created_at: 1.day.before, user_id: user.id, purchase_option_id: purchase_option.id, paid: purchase_option.price })

        post :create, params: request_params, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.body).to eq ({ purchase_option_id: ["Current purchase exists in database."]}.to_json )
        expect(response.content_type).to eq('application/json')
      end

      it "should purchase when user bought this content later than two days before" do
        PurchasedContent.create({ created_at: 3.days.before, user_id: user.id, purchase_option_id: purchase_option.id, paid: purchase_option.price })

        post :create, params: request_params, as: :json
        expect(response).to be_successful
      end
    end
  end
end
