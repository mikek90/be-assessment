require 'rails_helper'

RSpec.describe ContentsController, type: :controller do
  let(:user) { User.create({ email: "test@example.com" }) }
  let(:video_quality) { VideoQuality.create({ name: 'HD'}) }

  describe "GET #index" do
    before(:each) do
      (1..4).each do |i|
        movie = Movie.create({ name: "m#{i}", plot: "p m#{i}" })
        movie_purchase_option = PurchaseOption.create({content: movie, video_quality: video_quality, price: 2.99})
        PurchasedContent.create({ user_id: user.id, purchase_option_id: movie_purchase_option.id, paid: movie_purchase_option.price })

        season = Season.create({ name: "s#{i}", plot: "p s#{i}", number: i })
        season_purchase_option = PurchaseOption.create({content: season, video_quality: video_quality, price: 2.99})
        PurchasedContent.create({ user_id: user.id, purchase_option_id: season_purchase_option.id, paid: season_purchase_option.price })
        (1..2).each do |episode_no|
          Episode.create({ name: "s#{i}e#{episode_no}", plot: "p s#{i}e#{episode_no}", number: episode_no, parent_id: season.id })
        end
      end
    end

    it "returns bad request if type is different than movie, season, all" do
      get :index, params: {type: :bad}, as: :json
      expect(response.status).to eq 400
      expect(response.body).to eq ({ error: "Wrong type", status: 400}.to_json )
    end

    it "returns only movie types" do
      get :index, params: {type: :movie}, as: :json
      expect(response).to be_successful
      response_body = JSON.parse(response.body)
      returned_types = response_body['items'].map { |h| h['type'] }.uniq

      expect(returned_types.count).to eq 1
      expect(returned_types.first).to eq Content::MOVIE_TYPE
    end

    it "returns only season types" do
      get :index, params: {type: :season}, as: :json
      expect(response).to be_successful
      response_body = JSON.parse(response.body)
      returned_types = response_body['items'].map { |h| h['type'] }.uniq

      expect(returned_types.count).to eq 1
      expect(returned_types.first).to eq Content::SEASON_TYPE
    end

    it "returns season and movie types when get type is set to 'all'" do
      get :index, params: {type: :all}, as: :json
      expect(response).to be_successful
      response_body = JSON.parse(response.body)
      returned_types = response_body['items'].map { |h| h['type'] }.uniq

      expect(returned_types.count).to eq 2
      expect(returned_types).to match_array Content::PURCHASABLE_TYPES
    end

    describe "pagination" do
      it "returns all items if page parameter is not passed" do
        get :index, params: {type: :all}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body['items'].count).to eq 8
        expect(response_body['items'].count).to eq response_body['itemsCount']
      end

      it "returns 5 items for first page if pagesize param is not set" do
        get :index, params: {type: :all, page: 1}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body["items"].count).to eq 5
      end

      it "returns pagesize items for first page" do
        get :index, params: {type: :all, page: 1, pagesize: 3}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body["items"].count).to eq 3
      end

      it "returns last page" do
        get :index, params: {type: :all, page: 3, pagesize: 3}, as: :json
        expect(response).to be_successful
        response_body = JSON.parse(response.body)
        expect(response_body["items"].count).to eq 2
      end
    end
  end
end
