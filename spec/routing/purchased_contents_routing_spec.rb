require "rails_helper"

RSpec.describe PurchasedContentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/purchased_contents").to route_to("purchased_contents#index", format: :json)
    end

    it "routes to #show" do
      expect(:get => "/purchased_contents/1").to route_to("purchased_contents#show", :id => "1", format: :json)
    end


    it "routes to #create" do
      expect(:post => "/purchased_contents").to route_to("purchased_contents#create", format: :json)
    end
  end
end
