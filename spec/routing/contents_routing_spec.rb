require "rails_helper"

RSpec.describe ContentsController, type: :routing do
  describe "routing" do
    it "routes to #index with type" do
      ["movie", "season", "all"].each do |type|
        expect(:get => "/contents/#{type}").to route_to(controller: "contents", action: "index", type: type, format: :json)
      end
    end
  end
end
