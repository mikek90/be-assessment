# README

## Prepare environment
1. Run `bundle install`.
2. Stop your local Postgres database service if is working on **5432** port.
3. Run `docker-compose up -d`.
4. Run `rake db:create`.
5. Run `rake db:migrate`.
6. Run `rake db:seed`. This will feed database with sample data. Sample data contains movies, seasons, programs, purchase options and 10 users.

## Run tests
You can run tests by running `RAILS_ENV=test rspec` command.

## Run tests
Run rails server using `rails server` command,

## Main endpoints
### Contents
#### Movies ordered by creation.
`GET /contents/movie[?page=:number&pagenumber=:number]`

#### Seasons ordered by creation, including the list of episodes ordered by its number.
`GET /contents/season[?page=:number&pagenumber=:number]`

#### Movies and seasons, ordered by creation.
`GET /contents/all[?page=:number&pagenumber=:number]`

### Purchase content and library.
#### Purchase of a content.
```
POST /purchased_contents

params:
{
  "user_id": :number,
  "purchase_option_id": :number
}
```

#### Library of a user ordered by the remaining time to watch the content.
`GET /purchased_contents?user_id=1[&page=:number&pagenumber=:number]`
