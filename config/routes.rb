Rails.application.routes.draw do
  get 'contents/:type', action: :index, controller: :contents, defaults: { format: :json }

  resources :purchased_contents, only: [:index, :show, :create], defaults: { format: :json }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
